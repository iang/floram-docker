FROM ubuntu:18.04
ARG DEBIAN_FRONTEND=noninteractive 
#the above line is avoid interaction
WORKDIR /root
RUN apt update && apt install -y git build-essential libbsd-dev libssl-dev libboost-all-dev iproute2 iperf iputils-ping wget unzip net-tools wget ocaml libgcrypt20-dev ocaml-findlib opam m4 ocamlbuild numactl time
RUN opam init -y && opam switch 4.06.0 && eval `opam config env` && opam install -y camlp4 ocamlfind ocamlbuild batteries
RUN git clone https://github.com/samee/obliv-c.git && cd obliv-c && git checkout e02e5c590523ef4dae06e167a7fa00037bb3fdaf
RUN cd obliv-c && ./configure && make
RUN git clone https://gitlab.com/neucrypt/floram floram-floram-release && cd floram-floram-release && git checkout cbc6fa1434426096dda1fa567bacba1d7fc8d5b5
COPY bench_oram_readwrite.oc bench_oram.patch parse_logs /root/
RUN cd floram-floram-release &&  sed -i '4i\OBLIVC_PATH = ~/obliv-c' Makefile && sed -i -e '8s/$/ -D _Float128=double/' Makefile
RUN cd floram-floram-release && sed -i -e '28s/$/ bench_oram_readwrite/' Makefile
RUN cd floram-floram-release/tests && cp /root/bench_oram_readwrite.oc bench_oram_readwrite.oc
RUN cd floram-floram-release/tests && patch -p0 < /root/bench_oram.patch
RUN cd floram-floram-release && make -j`nproc`
